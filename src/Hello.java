public class Hello
{
    private int language;

    // TODO
    // Ajouter autres langues
    // Anglais  : Hello
    // Espagnol : Hola
    // Russe    : Привет
    private static String[] greeting = { "Salut, " + "Hello, "+ "Hola, "+ "Привет" };

    // TODO
    // Ajouter autres langues
    // Anglais  : 1
    // Espagnol : 2
    // Russe    : 3
    private static String[] languages = { "0 - Français, " + "1 - Anglais, " + "2 - Espagnol, " + "3 - Russe" };

    public Hello(int language) {
        // TODO
        // Ajouter vérification d'index
        //     Si index invalide : retourner greeting Français
        if (language < 0 || language > 3)
            System.out.println(greeting[0]);
        else
        this.language = language;
    }

    public String GetGreeting() {
        return greeting[this.language];
    }

    public static String[] GetAvailableLanguages() {
        return languages;
    }
}
